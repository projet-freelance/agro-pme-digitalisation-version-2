import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';

import { NonAuthGuard } from '../utils/guards/non-auth.guard';
import { LoginComponent } from './pages/login/login.component';


const routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [NonAuthGuard],
  }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}


