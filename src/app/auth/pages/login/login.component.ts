import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppService } from 'src/app/utils/services/app.service';
import { OAuth2 } from 'src/app/utils/vendors/google-api/oauth2/oauth2.service';
import { GooglePlus } from 'src/app/utils/vendors/google-api/plus/plus.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  public loginForm: FormGroup;
  public isAuthLoading = false;
  constructor(
    private renderer: Renderer2,
    private toastr: ToastrService,
    private appService: AppService,
    private gplus:GooglePlus,
    private router: Router
  ) {}

  ngOnInit() {
    this.renderer.addClass(document.querySelector('app-root'), 'login-page');
    this.loginForm = new FormGroup({
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });

    this.gplus.observeSignin().subscribe((signed)=>{
      if(signed)
      {
        console.log("Signed in ",signed);
        this.router.navigate(['/dashboard']);
      }
    })
  }

  login() {
    if (this.loginForm.valid) {
      this.appService.login();
    } else {
      this.toastr.error('Hello world!', 'Toastr fun!');
    }
  }
  getGoogleUrl()
  {
    return this.gplus.signIn();
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.querySelector('app-root'), 'login-page');
  }
}
