import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';

const routes = [
  { 
      path: 'dashboard',
      component: MainComponent,
      children: [
        { path: '', redirectTo:'home',pathMatch: 'full'},
      ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }
