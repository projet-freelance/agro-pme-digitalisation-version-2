import { viewClassName } from '@angular/compiler';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbToast } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map} from 'rxjs/operators';
import { getDateNow } from 'src/app/utils/functions/date';

import { Client } from 'src/app/utils/models/client.model';
import { EntityID } from 'src/app/utils/models/EntityID';
import { ClientService } from 'src/app/utils/services/client.service';
import { GDirectoryService } from 'src/app/utils/vendors/google-api/admin/directory.service';
import { GGroup } from 'src/app/utils/vendors/google-api/admin/group';
import { GDriveService } from 'src/app/utils/vendors/google-api/drive/drive.service';
import { Email } from 'src/app/utils/vendors/google-api/mail/email';
import { GMailService } from 'src/app/utils/vendors/google-api/mail/mail.service';
import { ResultStatut } from 'src/app/utils/vendors/google-api/resultstatut';
import { GTask } from 'src/app/utils/vendors/google-api/task/task';
import { ProcessusType } from '../../../utils/models/processustype';
import { MoteurProcessusService } from '../../../utils/services/moteurprocessus.service';
import { TaskService } from '../../../utils/services/task.service';
declare var DynamicTable:any;
declare var toastr: any;
declare var $: any;
@Component({
  selector: 'app-new-projet',
  templateUrl: './new-projet.component.html',
  styleUrls: ['./new-projet.component.scss']
})
export class NewProjetComponent implements OnInit, AfterViewInit {
  titlePage:String="Nouveau projet";
  path:any=[
    {
      'link':'/dashboard',
      'text':'Dashboard'
    },
    {
      'link':'#',
      'text':'Projet'
    },
    {
      'link':'#',
      'text':'Nouveau'
    }
  ];
  

  clientList:Client[]=[];
  selectedClient:any=null;

  responsableList:any[]=[];
  selectedResponsable:any=null;

  serviceList:GGroup[]=[];
  selectedService:any=null;
  listFileTable:any;

  formAddClient:FormGroup;
  submitFormAddClient:boolean=false;

  formAddProjet:FormGroup;
  submitFormAddProjet:boolean=false;
  projetFileList:Record<string,any>[];
  projet_type=[
    ProcessusType.SELECTION_PAR_ENTENTE_DIRECT,
    ProcessusType.APPEL_MANIFESTATION_INTERET,
    ProcessusType.SELECTION_PAR_QUALIFICATION
  ];

  
    
  constructor(private clientService: ClientService, 
      private processusService:MoteurProcessusService,
      private gmail:GMailService,
      private gdirectory:GDirectoryService,
      private dashboard:ElementRef,
      private taskService:TaskService
      ) { 
        this.listFileTable=new DynamicTable();
      }

  ngOnInit(): void {
    this.clientService.subscribeClientSubject().subscribe((clients)=>{
      this.clientList=clients;
    })
    this.gdirectory.groupSubject.subscribe((group)=>{
      this.serviceList=group;
    })
    this.gdirectory.userSubject.subscribe((user)=>{
      this.responsableList=user;
    })
    
    this.formAddClient = new FormGroup({
      nomPrenom: new FormControl('',[Validators.required,Validators.minLength(4)]),
      tel: new FormControl(''),
      email:new FormControl('',[Validators.email]),
      localisation:new FormControl('',[Validators.minLength(3)])
    })

    this.formAddProjet = new FormGroup({
      projetName: new FormControl('',[Validators.required]),
      descriptionProjet: new FormControl('',[Validators.required]),
      typeProjet: new FormControl('',[Validators.required]),
      serviceConcernProjet: new FormControl('',),
      responsableProjet: new FormControl('',),
      fileProjet:new FormControl('')
    })

  }
  
  ngAfterViewInit() {

    this.formAddProjet.controls.typeProjet.valueChanges.subscribe((value)=>{
      console.log("new Value ",value)
    })
    this.listFileTable.setTable(this.dashboard.nativeElement.querySelector('.table_fichier'));
    
    $('.client-select2').select2({
      templateSelection:(state)=>{
        this.selectedClient={
          nomPrenom:state.text,
          id:state.id
        }
        return state.text
      }
    });

    $('.service-select2').select2({
      templateSelection:(state)=>{
        this.selectedService=state;
        this.gdirectory.setSelectedGroup(this.selectedService.id);
        this.selectedResponsable=null;
        return state.text
      }
    })

    $('.responsable-select2').select2({
      templateSelection:(state)=>{
        this.selectedResponsable=this.responsableList.find((respond)=>respond.id==state.id)
        return state.text
      }
    });

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
    
  } 
  addClient()
  {
    
    this.submitFormAddClient=true;  
    this.clientService.add(Client.fromString({
      nomPrenom:this.formAddClient.value.nomPrenom,
      tel:this.formAddClient.value.tel,
      email:this.formAddClient.value.localisation,
      localisation:this.formAddClient.value.localisation
    }))
    .then((result: ResultStatut)=>{
      this.submitFormAddClient=false;
      toastr.success("Client enregistré avec success","Ajout de client",{timeOut:2000,"progressBar": true,})
      this.formAddClient.reset();
    })
    .catch((error: ResultStatut)=>{
      this.submitFormAddClient=false;
      
    })
  }

  addProjet()
  {
    this.submitFormAddProjet=true;  
    let listDoc=this.listFileTable.getValue().map((tabV)=> {
      return {
        "filename":tabV[0],
        "fileurl":tabV[1]
      }
    });
    let projetId=(new EntityID()).toString()
    this.processusService.newProcess({
      id:projetId,
      type:this.formAddProjet.controls.typeProjet.value,
      nomProjet:this.formAddProjet.controls.projetName.value,
      description:this.formAddProjet.controls.descriptionProjet.value,
      idClient:this.selectedClient.id,
      responsable:{
          service:this.selectedService.id,
          chefProjet:this.selectedResponsable.id
      },
      docs:listDoc
    })
    .then((result: ResultStatut)=>{
      let mail:Email=new Email();
      let mailBody=`Type de projet: ${this.formAddProjet.controls.typeProjet.value.text}\r\n
      Titre du projet: ${this.formAddProjet.controls.projetName.value}\r\n
      Client: ${this.selectedClient.nomPrenom}\r\n
      Description\r\n${this.formAddProjet.controls.descriptionProjet.value}\r\n`;
      
      if(listDoc.length>0)
      {
        mailBody+=`Documents source:\r\n`
        mailBody+=listDoc.map((doc)=>`* ${doc.filename} ${doc.fileurl}`).join("\r\n");
      }
      return this.gmail.sendMail(
        mail
        .from("me")
        .to(this.selectedResponsable.email)//
        .subject(`${this.formAddProjet.controls.projetName.value}`)
        .textContent(mailBody))
    })
    .then((result: ResultStatut)=>{
      let task:GTask=new GTask();
      task.title="Nouveau projet";
      task.notes=`Vous avez choisi comme chef du projet ${this.formAddProjet.controls.projetName.value}`;
      task.selfLink=`/projet/progress/${projetId}`;
      task.datesend=getDateNow().date;
      return this.taskService.addTask(task,this.selectedResponsable.id)
    })
    .then((result: ResultStatut)=>{
      this.submitFormAddProjet=false; 
      toastr.success("Projet enregistré avec success","Nouveau projet",{timeOut:2000,"progressBar": true,})
      setTimeout(() =>toastr.success(`${this.selectedResponsable.fullName} notifié par mail`,"Nouveau projet",{timeOut:2000,"progressBar": true,}),200);
      this.formAddProjet.reset();
    })
    .catch((error: ResultStatut)=>{
      this.submitFormAddClient=false;
      toastr.error("Erreur enregistrement du projet: <br>"+error.message,"Nouveau projet",{timeOut:2000,"progressBar": true,})
      console.log(error)
      this.submitFormAddProjet=false; 
    })
  }

}
