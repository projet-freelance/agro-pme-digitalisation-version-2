import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewProjetComponent } from './components/new-projet/new-projet.component';


export const routes: Routes = [
    { path:'', redirectTo:'new', pathMatch: 'full'},
    {
        path: 'new', 
        pathMatch: 'full',
        component: NewProjetComponent
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class GestionRoutingModule {}
