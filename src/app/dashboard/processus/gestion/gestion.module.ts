import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewProjetComponent } from './components/new-projet/new-projet.component';
import { GestionRoutingModule } from './gestion-routing.module';
import { BreadCrumbModule } from '../../components/breadcrumbs/breadcrumbs.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [NewProjetComponent],
  imports: [
    CommonModule,
    GestionRoutingModule,
    BreadCrumbModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class GestionModule { }
