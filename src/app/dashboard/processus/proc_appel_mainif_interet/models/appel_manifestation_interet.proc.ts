import { Activite } from './../../utils/models/activite'
import { ProcessusType } from './../../utils/models/processustype';
import { Processus } from './../../utils/models/processus';


import { Client } from 'src/app/utils/models/client.model';

export class ProcessusAppelManifestationInteret extends Processus
{
    constructor(nomProjet:String,description:String,idClient,dateDebut="",dateFin="")
    {
        super();
        this.idClient=idClient;
        this.description=description;
        this.nomProjet=nomProjet;
        this.dateDebut=dateDebut;
        this.dateFin=dateFin;
        this.type=ProcessusType.APPEL_MANIFESTATION_INTERET;
        this.constructWorkflow();
    }

    constructWorkflow(): void {
    
        /*let act1 = new Activite();
        act1.title="Reuinion d'identification des partenaires stratégiques";
        act1.description="Convocation d'une réuinion technique en vue d'identification les partenaires stratégiques";
        act1.executeur=CompteExecuteur.RSAF.nom;
        act1.idExecuteur=CompteExecuteur.RSAF.id;
        act1.formulaire="appel_manifestation_interet/convocation-reuinion/convocation-reuinion.component";
        this.workflow.push(act1);

        let act2 = new Activite();
        act2.title="Reparticition des rôles et des tâches";
        act2.description="Reparticition des rôles et des tâches entre les membres et l'équipe en charge de préparer la réponse à l'avis de manifestation";
        act2.executeur=CompteExecuteur.DIRECTEUR_GENERAL.nom;
        act2.idExecuteur=CompteExecuteur.DIRECTEUR_GENERAL.id;
        act2.formulaire="appel_manifestation_interet/repartition-role/repartition-role.component";
        this.workflow.push(act2);

        let act3 = new Activite();
        act3.title="Inventaire des pieces du dossier administratif";
        act3.description="Inventaire constituant les pieces du dossier administratif solicité par le client dans les termes de références";
        act3.executeur=CompteExecuteur.RSAF.nom;
        act3.idExecuteur=CompteExecuteur.RSAF.id;
        act3.formulaire="appel_manifestation_interet/piece-admin/piece-admin.component";
        this.workflow.push(act3);*/
    }
    static fromObject(obj:any):ProcessusAppelManifestationInteret
    {
        let proc=new ProcessusAppelManifestationInteret(obj.title,obj.description,obj.idClient,obj.dateDebut?obj.dateDebut:"",obj.dateFin?obj.dateFin:"");
        proc.fromObject(obj);
        return proc
    }
    
}