import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from '../components/main/main.component';
import { routes as GestionRoutes } from './gestion/gestion-routing.module';

const routes: Routes = [
    {
        path: 'process',
        component: MainComponent,
        children:[
            { 
                path: 'gestion',
                children:[...GestionRoutes]
            },
        ]
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class ProcessusRoutingModule {}
