import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GestionModule } from './gestion/gestion.module';
import { ProcessusRoutingModule } from './processus-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProcessusRoutingModule,
    GestionModule,
  ]
})
export class ProcessusModule { }
