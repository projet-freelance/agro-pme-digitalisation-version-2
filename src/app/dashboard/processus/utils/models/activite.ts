import { EntityID } from 'src/app/utils/models/EntityID';


export class Activite
{
    title:String="";
    description:String="";
    formulaire:String="";
    executeur:String="";
    idExecuteur:String="";
    dateExecution={};
    id:String=(new EntityID()).toString();
    data:any={};

    toString():any
    {
        let result={};
        result[this.id.toString()]={
            title:this.title,
            description:this.description,
            executeur:this.executeur,
            idExecuteur:this.idExecuteur,
            formulaire:this.formulaire,
            data:this.data,
            dateExecution:this.dateExecution,
        }
        return result;
    }
    
    static fromObject(obj:any):Activite
    {
        let activite:Activite=new Activite();
        let id=Object.keys(obj)[0];

        activite.id=id || (new EntityID()).toString();
        activite.title=obj[id]["title"] || "";
        activite.description=obj[id]["description"] || "";
        activite.executeur=obj[id]["executeur"] || "";
        activite.idExecuteur=obj[id]["idExecuteur"] || "";
        activite.formulaire=obj[id]["formulaire"] || "";
        activite.dateExecution=obj[id]["dateExecution"] || {};
        activite.data=obj[id]["data"] || {};
        return activite;
    }
}