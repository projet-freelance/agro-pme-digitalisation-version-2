import { EntityID } from 'src/app/utils/models/EntityID';
import { Activite } from "./activite";



export abstract class Processus
{
    type:any={};
    nomProjet:String="";
    description:String="";
    workflow:Activite[]=[];
    step=-1;
    idClient:String="";
    dateDebut="";
    dateFin=""
    idServiceResponsable:String="";
    idChefProjet:String
    docProjet:String[]=[];
    id:String=(new EntityID()).toString();
    
    getCurrentActivity():Activite
    {
        return this.workflow[this.step];
    }
    nextActivity():Activite
    {
        if(this.isEnd()) return null;
        this.step++;
        return this.getCurrentActivity();        
    }
    getDataActivity(index=-1):any
    {
        if(index==-1) return this.getCurrentActivity().data;
        if(index-1<0  || index-1>this.workflow.length) return null;
        return this.workflow[index-1].data;
    }
    isEnd():boolean
    {
        return this.step==this.workflow.length;
    }
    toString()
    {
        let workflowString=this.workflow.map((activite:Activite)=>activite.toString());
        return {
            id:this.id,
            type:this.type,
            nomProjet:this.nomProjet,
            description:this.description,
            date:{
                debut:this.dateDebut,
                fin:this.dateFin
            },            
            workflow:workflowString,
            step:this.step,
            idClient:this.idClient,
            responsable:{
                service:this.idServiceResponsable,
                chefProjet:this.idChefProjet
            },
            docs:this.docProjet
        }
    }
    fromObject(obj:any)
    {
        if(obj.workflow==undefined) obj.workflow=[];
        this.workflow= obj.workflow.map(activite=>Activite.fromObject(activite));
        this.step=obj.step?obj.step:-1;
        this.id=obj.id?obj.id:(new EntityID()).toString();
        this.nomProjet=obj.nomProjet?obj.nomProjet:"";
        this.type=obj.type?obj.type:{};
        this.description=obj.description?obj.description:"";
        if(obj.date)
        {
            this.dateDebut=obj.date.debut?obj.date.debut:"";
            this.dateFin=obj.date.fin?obj.date.fin:"";
        }
        this.idClient=obj.idClient?obj.idClient:"";
        this.idServiceResponsable=obj.responsable.service;
        this.idChefProjet=obj.responsable.chefProjet;
        this.docProjet=obj.docs?obj.docs:[];
    }
    abstract constructWorkflow():void;
}

