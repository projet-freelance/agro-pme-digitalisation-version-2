export class ProcessusType
{
    static SELECTION_PAR_QUALIFICATION={
        id:"selection_par_qualification",
        text:"Sélection par qualification"
    };
    static SELECTION_PAR_ENTENTE_DIRECT={
        id:"selection_par_entente_direct",
        text:"Sélection par entente direct"
    };
    static APPEL_MANIFESTATION_INTERET= {
        id:"appel_manifestation_interet",
        text:"Appar a manifestation d'intérêt"
    };
}