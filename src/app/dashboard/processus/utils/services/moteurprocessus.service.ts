import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { ProcessFactory } from './processfactory';
import { Processus } from '../models/processus';
import { getDateNow } from 'src/app/utils/functions/date';
import { Client } from 'src/app/utils/models/client.model';
import { FirebaseApi } from 'src/app/utils/vendors/google-api/firebase/FirebaseApi';
import { ResultStatut } from 'src/app/utils/vendors/google-api/resultstatut';


@Injectable({
  providedIn: 'root'
})
export class MoteurProcessusService {
  protected proccessList:Map<String,Processus>=new Map();
  processListSubject:any=new Subject<any[]>();

  constructor(private fetchApi:FirebaseApi) {
    this.fetchApi.fetch("/projets")
    .then((result:ResultStatut)=>{
      for(let prok in result.result)
      {
        console.log("Processus ",prok,result.result[prok])
          this.proccessList.set(result.result[prok].id,ProcessFactory.getInstanceFromObject(result.result[prok]));
      }
      this.emit();
    });
  }
  subscribeToProcess()
  {
    return this.processListSubject;
  }

  emit():void
  {
    this.processListSubject.next(this.getProcessList());
  }
  newProcess(processObj):Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>
    {
      let proc:Processus=ProcessFactory.getInstanceFromObject(processObj);

      this.fetchApi.set(`/projets/${proc.id}`,proc.toString())
      .then((result:ResultStatut)=>{
        this.proccessList.set(proc.id,proc);
        result.result=proc;
        this.emit()
        resolve(result);
      })
      .catch((error:ResultStatut)=>reject(error));
    });
  }
  nextProcessStep(processId:String,data:any):Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve,reject)=>
    {
      let result:ResultStatut=new ResultStatut();
      if(processId==undefined || processId==null)
      {
        result.apiCode=ResultStatut.INVALID_ARGUMENT_ERROR;
        result.message=`L'identifiant du processus ne peut être vide`;
        reject(result);
      }
      if(!this.proccessList.has(processId))
      {
        result.apiCode=ResultStatut.RESSOURCE_NOT_FOUND_ERROR;
        result.message=`Processus d'indentification ${processId} est introuvable`;
        reject(result);
      }

      let currActivity = this.proccessList.get(processId).getCurrentActivity();
      currActivity.data=data;
      currActivity.dateExecution=getDateNow()
      result.result=this.proccessList.get(processId).nextActivity();
      this.fetchApi.set(`/projets/${processId}`,this.proccessList.get(processId).toString())
      .then((r:ResultStatut)=>{
        resolve(result);
      })
      .catch((error:ResultStatut)=>{
        this.fetchApi.handleApiError(error);
        reject(error)
      });
      resolve(result);
      
    });
  }

  getProcess(processId):Processus
  {
    if(processId==undefined || !this.existProcess(processId)) return null;
    return this.proccessList.get(processId);
  }
  existProcess(processId:String):boolean
  {
    if(processId==undefined) return false;
    return this.proccessList.has(processId.toString());
  }
  getProcessList()
  {
    let procL=[];
    for(let poc of this.proccessList.keys())
    {
      procL.push(this.proccessList.get(poc));
    }
    return procL;
  }
}
