import { ProcessusAppelManifestationInteret } from '../../proc_appel_mainif_interet/models/appel_manifestation_interet.proc';
import { Processus } from '../models/processus';
import { ProcessusType } from '../models/processustype';

export class ProcessFactory
{
   static getInstance(processType:String,title,description,client,dateDebut,dateFin):Processus
   {
   /* if(processType==ProcessusType.SELECTION_PAR_ENTENTE_DIRECT.id)
    {
      return new ProcessusSelectionParEntenteDirect(title,description,client,dateDebut,dateFin);
    }
    else if(processType==ProcessusType.SELECTION_PAR_QUALIFICATION.id)
    {
      return new ProcessusSelectionParQualificationConsultant(title,description,client,dateDebut,dateFin);
    }
    else*/ if(processType==ProcessusType.APPEL_MANIFESTATION_INTERET.id)
    {
      return new ProcessusAppelManifestationInteret(title,description,client,dateDebut,dateFin);
    }
    return null;
   } 
   static getInstanceFromObject(entity:Record<string,any>):Processus
   {
     console.log("Proc",entity)
    /*if(entity.type.id==ProcessusType.SELECTION_PAR_ENTENTE_DIRECT.id)
    {
      return ProcessusSelectionParEntenteDirect.fromObject(entity);
    }
    else if(entity.type.id==ProcessusType.SELECTION_PAR_QUALIFICATION.id)
    {
      return ProcessusSelectionParQualificationConsultant.fromObject(entity);
    }
    else */if(entity.type.id==ProcessusType.APPEL_MANIFESTATION_INTERET.id)
    {
      return ProcessusAppelManifestationInteret.fromObject(entity);
    }
    return null;
   }
}