import { Injectable } from '@angular/core';
import { toJSDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-calendar';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/utils/services/api.service';
import { FirebaseApi } from 'src/app/utils/vendors/google-api/firebase/FirebaseApi';
import { GooglePlus } from 'src/app/utils/vendors/google-api/plus/plus.service';
import { ResultStatut } from 'src/app/utils/vendors/google-api/resultstatut';
import { GTask } from 'src/app/utils/vendors/google-api/task/task';
declare var gapi: any;
@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private taskClient=null;
  private authUser=null;
  private activeTask:GTask[]=[];
  private pastTask:GTask[];
  activeTaskSubjet:any=new Subject<GTask[]>();
  pastTaskSubject:any=new Subject<GTask[]>();


  constructor(private firebase:FirebaseApi,private googlePlus:GooglePlus)
    {
        this.googlePlus.observeSignin().subscribe((signed)=>{
            if(signed)
            {
              this.authUser = this.googlePlus.getAccount();
            }
        })
    }
    initApi()
    {
        this.activeTask=[];
        this.pastTask=[];
        this.getTaskListFromApi("active");
        this.getTaskListFromApi("past");
    }
    getTaskListFromApi(taskList)
    {
        this.firebase.fetch(`/users/${this.authUser.getId()}/tasks/${taskList}`)
        .then((result)=>{
            for(let key in result.result)
            {
                if(taskList=="active") {
                    this.activeTask.push(GTask.fromObjet(result.result[key]))
                }
                else this.pastTask.push(GTask.fromObjet(result.result[key]))                
            }
            this.emitTask(taskList);
            
        })
    }
    emitTask(taskList)
    {
        if(taskList=="active") this.activeTaskSubjet.emit(this.activeTask.slice())
        else this.pastTaskSubject.emit(this.pastTask.slice());
    }
    addTask(task:GTask,taskList="active",to:String=this.authUser.getId()):Promise<ResultStatut>
    {
        return new Promise<ResultStatut>((resolve, reject) =>{
            this.firebase.set(`/users/${to}/tasks/${taskList}/${task.id}`,task.toString())
            .then((result: ResultStatut)=>{
                this.emitTask(taskList);
                resolve(result)
            })
            .catch((error: ResultStatut)=>{
                this.firebase.handleApiError(error);
                reject(error);
            })
        })
    }
    markTaskAsDone(task:GTask,to:String=this.authUser.getId()):Promise<ResultStatut>
    {
        return new Promise<ResultStatut>((resolve, reject) =>{
            this.firebase.delete(`/users/${to}/tasks/active/${task.id}`)
            .then((result: ResultStatut)=>this.addTask(task,"past"))
            .then((result: ResultStatut)=>{
                let posTask=this.activeTask.findIndex((t)=>t.id==task.id);
                if(posTask>-1) this.activeTask.splice(posTask,1);
                this.pastTask.push(task);
                this.emitTask("active");
                this.emitTask("past");

                resolve(result)
            })
            .catch((error: ResultStatut)=>{
                this.firebase.handleApiError(error);
                reject(error);
            })
        })
    }

    
}
