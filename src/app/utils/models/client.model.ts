import { EntityID } from './EntityID';

export class Client
{
    id:String=(new EntityID()).toString();
    nomPrenom:String="";
    tel:String="";
    email:String="";
    localisation:String="";

    toString()
    {
        return {
            id:this.id,
            nomPrenom:this.nomPrenom,
            tel:this.tel,
            email:this.localisation,
            localisation:this.localisation
        }
    }

    static fromString(entity):Client
    {
        let client:Client = new Client();
        client.id = entity.id?entity.id:(new EntityID()).toString()
        client.email=entity.email?entity.email:"";
        client.localisation = entity.localisation?entity.localisation:"";
        client.nomPrenom = entity.nomPrenom?entity.nomPrenom:"";
        client.tel= entity.tel?entity.tel:"";
        return client;
    }
}