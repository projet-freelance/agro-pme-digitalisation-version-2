import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { GooglePlus } from '../vendors/google-api/plus/plus.service';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  public user = {
    firstName: 'Invalid',
    lastName: 'Invalid',
    image: 'assets/img/user2-160x160.jpg',
    id: "",
    email:"AGRO-PME EMAIL"
  };
  private useGoogleAccount=false;

  constructor(private router: Router, private googlePlus:GooglePlus) {
    this.googlePlus.observeSignin().subscribe((signed)=>{
      if(signed)
      {
        let account=this.googlePlus.getAccount();
        account = account.getBasicProfile();
        this.user.firstName=account.getFamilyName();
        this.user.lastName=account.getGivenName();
        this.user.image=account.getImageUrl();
        this.user.id=account.getId()
        this.user.email=account.getEmail();
        this.useGoogleAccount=true;
      }
    })
    
  }

  login() {
    localStorage.setItem('token', 'LOGGED_IN');
    this.router.navigate(['/']);
  }

  register() {
    localStorage.setItem('token', 'LOGGED_IN');
    this.router.navigate(['/']);
  }

  logout() {
    localStorage.removeItem('token');
    if(this.useGoogleAccount) this.googlePlus.signOut();
    this.router.navigate(['/login']);
  }
}
