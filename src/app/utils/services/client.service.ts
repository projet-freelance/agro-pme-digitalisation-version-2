import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Client } from '../models/client.model';
import { FirebaseApi } from '../vendors/google-api/firebase/FirebaseApi';
import { ResultStatut } from '../vendors/google-api/resultstatut';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  clientList:Client[]=[];
  clientListSubject=new Subject<Client[]>();

  constructor(private fetchApi:FirebaseApi) { 
    this.getClientFromAPI()
      .then((result: ResultStatut) => {
        this.clientList = [];
        console.log("Client ",result)
        for (let key in result.result) {
          // console.log("result", result.result[key])
          if (result.result[key]) this.clientList.push(Client.fromString(result.result[key]));
        }
        result.result = [];
        // console.log(this.courierEntrant);
        this.emit();
      });
  }
  emit(): void {
    this.clientListSubject.next(this.clientList.slice());

  }
  getClientFromAPI():Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve, reject) => {
      this.fetchApi.fetch(`/clients`)
        .then((result: ResultStatut) => resolve(result))
        .catch((error: ResultStatut) => reject(error));
    });
  }
  add(client:Client):Promise<ResultStatut>
  {
    return new Promise<ResultStatut>((resolve, reject) => {
      this.fetchApi.set(`/clients/${client.id}/`, client.toString())
        .then((result: ResultStatut) => {
          this.clientList.push(client);
          this.emit();
          resolve(result)
        })
        .catch((error: ResultStatut) => {
          this.fetchApi.handleApiError(error)
          reject(error)
        });
    });
  }
  getClient(clientID:String):Client
  {
    let client=this.clientList.find((clt)=>clt.id==clientID);
    if(client==undefined) return null;
    return client;
  }
  subscribeClientSubject() {
    return this.clientListSubject;
  }
}
