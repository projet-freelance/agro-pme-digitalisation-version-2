const apiaccess = {
    'drive':[
        "https://www.googleapis.com/auth/drive",
        "https://www.googleapis.com/auth/drive.file",
        "https://www.googleapis.com/auth/drive.metadata",
    ],
    'firebase':[
        "https://www.googleapis.com/auth/firebase"
    ],
    'mail':[
        "https://www.googleapis.com/auth/gmail.readonly",
        "https://www.googleapis.com/auth/gmail.send"
    ],
    'oauth':[
        "https://www.googleapis.com/auth/userinfo.email",
        "https://www.googleapis.com/auth/userinfo.profile"
    ],
    'calendar':[
        "https://www.googleapis.com/auth/calendar"
    ],
    'directory':[
        'https://apps-apis.google.com/a/feeds/groups/',
        'https://www.googleapis.com/auth/admin.directory.group',
        'https://www.googleapis.com/auth/admin.directory.group.readonly',
        'https://www.googleapis.com/auth/admin.directory.group.member',

        'https://www.googleapis.com/auth/admin.directory.user',
        'https://www.googleapis.com/auth/admin.directory.user.readonly',
        'https://www.googleapis.com/auth/cloud-platform'
    ],
    "contact":[
        'https://www.googleapis.com/auth/contacts'
    ],
    "task":[
        "https://www.googleapis.com/auth/tasks"
    ]
    
};

const discovery_doc=[
    "https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest",
    "https://www.googleapis.com/discovery/v1/apis/admin/directory_v1/rest",
    "https://admin.googleapis.com/$discovery/rest?version=reports_v1",
    "https://www.googleapis.com/discovery/v1/apis/tasks/v1/rest"
]

const credentiels = {
    "cliendID" : "296617121725-cda9cf6ppgb6umc9j7nseto0qqgd082a.apps.googleusercontent.com",
    "secret" : "qEf41M4PcmPCTukOrtHXFRr7",
    'apiKey' :'AIzaSyACPJGEZZljfipl7f2NKRXUelAa2x9i5Zg',
    "redirect": "http://localhost:4200/dashboard"
}
export { apiaccess , credentiels, discovery_doc};