import { Injectable } from '@angular/core';
import { ResultStatut } from '../resultstatut';
import { HttpClient } from '@angular/common/http';
import { OAuth2 } from '../oauth2/oauth2.service';
import { credentiels } from '../access.conf';
import { GGroup } from './group';
import { Subject } from 'rxjs';


declare var gapi: any;
@Injectable({
  providedIn: 'root'
})
export class GDirectoryService {
    private gdirectoryClient=null;
    private groups:GGroup[]=[];
    private userGroup:Map<String,Record<string,any>[]>=new Map();
    selectedGroup:String;
    groupSubject=new Subject<any[]>();
    userSubject=new Subject<any[]>();

    constructor() { 
     
    }
    initApi()
    {
      this.gdirectoryClient=gapi.client.directory;  
      this.getListGroupFromApi();      
    }
    getListGroupFromApi()
    {
      this.gdirectoryClient.groups.list({
        "customer": "my_customer",
        "domain": "agropme.cm"
      }).then((response)=> {
        response.result.groups.forEach(group => {
          this.groups.push(GGroup.fromObject(group));
          this.emitGroup();
          this.gdirectoryClient.members.list({
            "customer": "my_customer",
            "domain": "agropme.cm",
            "groupKey":group.id
          })
          .then((response)=>{
            response.result.members.forEach((member)=>{
              this.gdirectoryClient.users.get({
                "domain": "agropme.cm",
                "userKey":member.id
              }).then((result)=>{
                let user={
                  id:result.result.id,
                  email:result.result.primaryEmail,
                  fullName:result.result.name.fullName
                }
                if(!this.userGroup.has(group.name))
                {
                  this.userGroup.set(group.name,[user])
                }
                else this.userGroup.get(group.name).push(user);
                this.selectedGroup=this.groups[0].name;
                this.emitUser();
              })
            })
          })

        });       
        
      }).catch((err)=>console.log("Error Group ",err));
    }
    getListUser()
    {
        this.gdirectoryClient.groups.list({
          "customer": "my_customer",
          "domain": "agropme.cm"
        }).then(function(response) {
          console.log("Group",response.result)
          
        }).catch((err)=>console.log("Error Group ",err));

     
    }
    emitGroup()
    {
      this.groupSubject.next(this.groups.slice());
    }
    emitUser()
    {
      this.userSubject.next(this.userGroup.get(this.selectedGroup).slice());
    }
    setSelectedGroup(groupeName)
    {
      this.selectedGroup=groupeName;
      this.emitUser();
    }

}