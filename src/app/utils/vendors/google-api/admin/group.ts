export class GGroup
{
    adminCreated:boolean;
    description:String="";
    email:String="";
    id:String="";
    name:String="";

    static fromObject(obj:any):GGroup
    {
        let group=new GGroup();
        group.adminCreated=obj.adminCreated;
        group.description=obj.description;
        group.email=obj.email;
        group.name=obj.name;
        return group;
    }
}