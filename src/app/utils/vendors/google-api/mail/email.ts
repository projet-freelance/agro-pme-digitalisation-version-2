export class Email
{
    private sender:String="";
    private receiver:String="";
    private titleEmail:String="";
    private otherReceiver:String[]=[];
    private content:String[]=[];

    from(userFrom:String):Email
    {
        this.sender=userFrom;
        return this;
    }
    to(userTo:String):Email
    {
        this.receiver = userTo;
        return this;
    }
    cc(ccUser:String):Email
    {
        this.otherReceiver.push(ccUser);
        return this;
    }
    subject(titleMail:String):Email
    {
        this.titleEmail=titleMail;
        return this;
    }
    htmlContent(content:String):Email
    {
        this.content.push(content);
        return this;
    }
    textContent(content:String):Email
    {
        this.content.push(content);
        return this;
    }
    fileContent(file:any):Email
    {
        this.content.push(file);
        return this;
    }
    toObject()
    {
        return {
            "From":this.sender,
            "To":this.receiver,
            "Subject":this.titleEmail,
            "cc":this.otherReceiver,
            "body":this.content
        }
    }
    toString():String
    {
        let email:String="";
        email=`From : ${this.sender}\r\n`;
        email+=`To : ${this.receiver}\r\n`;
        if(this.otherReceiver.length>0)
        {
            email+=this.otherReceiver.map((cc)=> `cc : ${cc}\r\n`).join("");
        }
        email+=`Subject : ${this.titleEmail}\r\n\r\n`;
        email+=this.content.join("");
        return email;
    }
}