import { Injectable } from '@angular/core';
import { ResultStatut } from '../resultstatut';
import { HttpClient } from '@angular/common/http';
import { OAuth2 } from '../oauth2/oauth2.service';
import { Email } from './email';
import { GooglePlus } from '../plus/plus.service';
declare var gapi: any;
@Injectable({
  providedIn: 'root'
})
export class GMailService {
  private gmailClient=null;
  constructor()
    {

    }
    initApi()
    {
        this.gmailClient=gapi.client.gmail;
        console.log("Mail Client",this.gmailClient)
    }
    sendMail(mail:Email):Promise<ResultStatut>
    {
      return new Promise<ResultStatut>((resolve, reject) =>{
        this.gmailClient.users.messages.send({
          userId:mail.toObject().From,
          resource:{
            raw:btoa(mail.toString().toString())
          }
        }).then((result) => {
          resolve(result)
        }).catch((err) => {
          let result:ResultStatut=new ResultStatut();
          result.message="error";
          result.apiCode=ResultStatut.UNKNOW_ERROR;
          result.result=err;
          reject(err);
        });
      })    

    }
}
