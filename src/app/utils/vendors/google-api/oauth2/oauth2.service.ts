import { Injectable } from '@angular/core';
import  { google } from 'googleapis'
import { credentiels,apiaccess, discovery_doc } from './../access.conf'
import { ResultStatut } from './../resultstatut';

declare var gapi: any;

@Injectable({
    providedIn: 'root'
})
export class OAuth2
{
    private oauthClient=null;
    private googeURL=null;

    constructor()
    {
    }
    getOAuthClient()
    {
        return this.oauthClient;
    }
    getAuthClient():Promise<ResultStatut>
    {
        //this.oauthClient = new google.auth.OAuth2(credentiels.cliendID,credentiels.secret,credentiels.redirect);
        return new Promise<ResultStatut>((resolve,reject)=>{
            let result:ResultStatut=new ResultStatut();
            let scopes="";
            for(let scopek in apiaccess)
            {
                scopes+=" "+apiaccess[scopek].join(" ");
            }
            console.log("scopes "+scopes)
            gapi.load('client:auth2',()=>{
                gapi
                .client
                .init({
                    apiKey:credentiels.apiKey,
                    clientId:credentiels.cliendID,
                    scope:scopes,
                    discoveryDocs:discovery_doc
                    })
                .then(()=>{
                        this.oauthClient=gapi.auth2.getAuthInstance();               
                        /*this.oauthClient.isSignedIn.listen((sign)=>{
                            if(sign) resolve(sign)
                            else{
                                result.apiCode=ResultStatut.INVALID_ARGUMENT_ERROR;
                                reject(result)
                            }
                        });*/
                        console.log("googleClient",gapi.client)
                        result.result={
                            'authClient':this.oauthClient,
                            'gapi':gapi
                        };
                        resolve(result);
                    })
                .catch((error)=>{
                        result.apiCode=ResultStatut.UNKNOW_ERROR;
                        result.result=error;
                        result.message="error";
                        reject(result);
                })
            })
        });
    }
    private getConnectionUrl()
    {
        this.googeURL=this.oauthClient.generateAuthUrl({
            access_type:'offline',
            prompt:"consent",
            scope: apiaccess
        })
    }

    getGoogleUrl()
    {
        return this.getGoogleUrl;
    }
}