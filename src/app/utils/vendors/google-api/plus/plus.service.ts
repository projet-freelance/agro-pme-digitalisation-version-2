import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { GDirectoryService } from '../admin/directory.service';
import { GPeopleService } from '../contact/people.service';
import { GMailService } from '../mail/mail.service';
import { OAuth2 } from '../oauth2/oauth2.service';
import { credentiels,apiaccess } from './../access.conf'
import { ResultStatut } from './../resultstatut';

@Injectable({
    providedIn: 'root'
})
export class GooglePlus
{
    private signed:boolean=false;
    private currentUser=null;
    private authClient=null;
    private signedObservable:Subject<boolean>=new Subject<boolean>();

    constructor(
        private oauth2:OAuth2,
        private gmail:GMailService,
        private gdirectory:GDirectoryService,
        private gpeople:GPeopleService)
    {
        this.oauth2.getAuthClient().then((result:ResultStatut)=>{

            this.authClient=result.result.authClient; 

            if(this.authClient.isSignedIn.get())
            {
                console.log("Is signin",this.authClient.currentUser.get())
                this.signed=true;
                this.currentUser=this.authClient.currentUser.get()
                this.signedObservable.next(true);
                this.gmail.initApi();
                this.gdirectory.initApi();
                this.gpeople.initApi();
            }

            this.authClient.isSignedIn.listen((sig)=>{
                this.signed=sig
                console.log("signed",this.signed);
                console.log("user: ",this.authClient.currentUser.get())                
                if(sig) {
                    this.currentUser=this.authClient.currentUser.get()
                    this.gmail.initApi();
                    this.gdirectory.initApi();
                    this.gpeople.initApi();
                }
                this.signedObservable.next(sig);
            });
        }).catch((err:ResultStatut)=>console.log(err));
    }
    oauth2AuthClient()
    {
        return this.authClient;
    }
    observeSignin()
    {
        return this.signedObservable;
    }
    isSigned()
    {
        return this.signed;
    }
    signIn()
    {
        this.authClient.signIn();
    }
    signOut()
    {
        
        this.authClient.signOut();
    }
    getAccount()
    {
        return this.currentUser;
    }
}