import { Injectable } from '@angular/core';
import { ResultStatut } from '../resultstatut';
import { HttpClient } from '@angular/common/http';
import { OAuth2 } from '../oauth2/oauth2.service';
import { GooglePlus } from '../plus/plus.service';
import { GTask } from './task';
import { Subject } from 'rxjs';
declare var gapi: any;
@Injectable({
  providedIn: 'root'
})
export class GTaskService {
  private taskClient=null;
  private activeTask:GTask[]=[];
  private pastTask:GTask[];
  activeTaskSubjet:any=new Subject<GTask[]>();
  pastTaskSubject:any=new Subject<GTask[]>();

  constructor()
    {

    }
    initApi()
    {
        this.taskClient=gapi.client.task;
        this.getTaskListFromApi("active");
        this.getTaskListFromApi("past");
    }
    getTaskListFromApi(taskList)
    {
        this.taskClient.task.task.list({
            taskList:taskList
        })
        .then((result)=>{
            result.resource.items.forEach((task)=>{
                if(taskList=="active") this.activeTask.push(GTask.fromObjet(task))
                else this.pastTask.push(GTask.fromObjet(task))
                this.emitTask(taskList);
                
            })
        })
    }
    emitTask(taskList)
    {
        if(taskList=="active") this.activeTaskSubjet.emit(this.activeTask.slice())
        else this.pastTaskSubject.emit(this.pastTask.slice());
    }
    markTaskAsDone(task:GTask)
    {

    }
    addTask

    
}
