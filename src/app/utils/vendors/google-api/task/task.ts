import { EntityID } from 'src/app/utils/models/EntityID';

export class GTask
{
    id:String=(new EntityID()).toString();
    title:String="";
    update:String="";
    selfLink:String="";
    parent:String="";
    notes:String="";
    due:String="";
    completed:String="";
    deleted:String="";
    hidden:String="";
    datesend:String=""

    static fromObjet(obj):GTask
    {
        let gtask=new GTask();
        gtask.id=obj.id;
        gtask.title=obj.title;
        gtask.update=obj.update;
        gtask.selfLink=obj.selfLink;
        gtask.parent=obj.parent;
        gtask.notes=obj.notes;
        gtask.due=obj.due;
        gtask.completed=obj.completed;
        gtask.deleted=obj.deleted;
        gtask.hidden=obj.hidden;
        gtask.datesend=obj.datesend;
        return gtask;
    }
    toString()
    {
        return {
            id:this.id,
            title:this.title,
            update:this.update,
            selfLink:this.selfLink,
            parent:this.parent,
            notes:this.notes,
            due:this.due,
            completed:this.completed,
            deleted:this.deleted,
            hidden:this.hidden,
            datesend:this.datesend
        }
    }
}