class DynamicTable
{
    constructor(table=null,
        {
            editable=true,
            edition={
                html:'input',
                type:'text',
                htmls:[]
            }
        }={}
    )
    {
        console.log("table ",table);
        this.table=table;
        this.options={editable,edition:{...edition}};
        this.init();
        this.value=[];

    }
    changeTextToHtml(node,position)
    {
        let text=node.textContent;
        //console.log('html',this.options.edition.html);
        if( (this.options.edition.htmls instanceof Array) && (this.options.edition.htmls.length>0) )
        {

        }
        else if(this.options.edition.html==='input')
        {
            node.innerHTML=`<input type="${this.options.edition.type}" value=${text} />`;
        }
        else if(this.options.edition.html==='select')
        {
            let defaultValue=this.options.edition.defaultoption?this.options.edition.defaultoption:
                this.options.edition.options[0];
            let content=`<select> <option value="" disabled selected>${defaultValue}</option>`;
            this.options.edition.options.forEach((option)=>
            {
                content+=`<option value="${option}">${option}</option>`;
            });
            content+=`</select>`;
            node.innerHTML=content;    
            console.log("to edit",this.options.edition.html,content,node)        
        }
        else if(this.options.edition.html instanceof Array) 
        {
            node.innerHTML=this.options.edition.html[position];
            console.log("to edit",position,this.options.edition.html,this.options.edition.html[position],node)
        }
        else throw new Error("Dynamic-Table: Invalid html for edition");
        node.firstElementChild.focus();
        
    }
    changeHtmlToText(node)
    {
        let text=node.querySelector('input').value;
        node.innerHTML=text;
        console.log("html to text");
    }
    addEventListenerToCell(cell,position)
    {
        cell.addEventListener('dblclick',()=> this.changeTextToHtml(cell,position));
        cell.addEventListener('focusout',()=> this.changeHtmlToText(cell));
        cell.addEventListener('keypress',(e)=>{
            if(e.keyCode===13) this.changeInputToText(cell);
        })
    }
    setValue(tabValue)
    {
        let tbody=this.table.querySelector('.dynamic-table tbody');
        let tr="<tr>"
        tabValue.forEach(td=>{
            tr+="<td>"+td+"</td>";
        });
        tr+="</tr>";
        tbody.innerHTML+=tr;
    }
    getValue()
    {
        let tval=[]; 
        if(this.table == null || this.table==undefined) return tval;                     
        this.table.querySelectorAll('.dynamic-table tbody tr')            
        .forEach((tr)=>
        {
            let tdVal=[];
            let td=tr.querySelectorAll('td');
            for(let i=0;i<td.length; i++)
            {
                tdVal.push(td[i].textContent);
            }
            tval.push(tdVal);
        });
        return tval;
    }
    setTable(table)
    {
        this.table=table;
        this.init();
    }
    init()
    {
        if(this.table==null || this.table==undefined) return;
        if(this.options.editable)
        {
            //event to show botton
            this.table.addEventListener('mouseenter',(e)=>{
                this.table.querySelector('.dynamic-table-options').style.display="block";
            });

            //event to hide botton
            this.table.addEventListener('mouseleave',(e)=>
            {
                this.table.querySelector('.dynamic-table-options').style.display="none";
            });

            //event to remove table row
            this.table.querySelector('.dynamic-table-options .remove_cell').addEventListener('click',()=>
            {
                if(this.table.querySelector('table tbody').childElementCount>0)
                {
                    this.table.querySelector('table').deleteRow(-1);   
                }
            });

                //event to add table row
                this.table.querySelector('.dynamic-table-options .add_cell').addEventListener('click',()=>
                {
                    let lastRow=this.table.querySelector('table thead tr:last-child');
                    let newrow=this.table.querySelector('table tbody').insertRow(-1);
                    for(let i=0; i<lastRow.childElementCount;i++)
                    {
                        let newCell=newrow.insertCell(i);
                        newCell.textContent="";
                        this.addEventListenerToCell(newCell,i);
                    }
                });
            
            //add Event to cell
            this.table.querySelectorAll('tbody tr td')
            .forEach((td)=>
            {
                this.addEventListenerToCell(td,td.cellIndex);
            });
        }
    }
}